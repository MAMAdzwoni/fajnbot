const botSettings = require("./botsettings.json");
const packagejson = require("./package.json");
const Discord = require('discord.js');
const { Client, Attachment, RichEmbed } = require('discord.js');

// Create an instance of a Discord client
const client = new Discord.Client();

client.on('ready', () => {
    console.log(`${client.user.tag} działa`);
    console.log('Wersja: ' + packagejson.version )
    inviteurl = `https://discordapp.com/oauth2/authorize?&client_id=${client.user.id}&scope=bot&permissions=8`;
    client.user.setStatus( botSettings.status );
    ustaw_status();
});
client.on('guildCreate', guild => {
    ustaw_status();
});

client.on('guildDelete', guild => {
    ustaw_status();
});

function ustaw_status() {
    if (client.guilds.size == 1) {
        client.user.setActivity(`Bot na 1 serwerze | Prefix: ` + botSettings.prefix + '| Wersja: ' + packagejson.version , { type: 'PLAYING' });
    } else {
        client.user.setActivity(`Bot na ${client.guilds.size} serwerach | Prefix: ` + botSettings.prefix + '| Wersja: ' + packagejson.version , { type: 'PLAYING' });
    }
}

client.on('message', message => {
  if (message.content === botSettings.prefix + 'help') {
    const embedtext = new RichEmbed()
    var embed = new Discord.RichEmbed;
      embed.setTitle('Pomoc bota:')
      // Set the color of the embed
      embed.setColor(0x649a4e)
      // Set the main content of the embed
      embed.setDescription('**Prefix bota: **' + botSettings.prefix +'\n**Komendy:**\n`gitlab`\n`strona`\n`XD`\n**Reakcje na teksty:**\n`ok`');
      embed.setFooter('*Niektóre rzeczy są zaporzyczone z innych botów, aby się z nimi zapoznać wejdź na https://gitlab.com/MAMAdzwoni/fajnbot*')
    // Send the embed to the same channel as the message
    message.channel.send(embedtext);
  }
});

client.on('message', message => {
    if (message.content === botSettings.prefix + 'XD') {
        // Create the attachment using Attachment
        const attachment = new Attachment('img/XD.gif');
        // Send the attachment in the message channel
        message.channel.send(attachment);
    }
});
client.on('message', message => {

  if (message.content === 'ok') {

    message.channel.send(':ok_hand:');
  }
});
client.on('message', message => {
    if (message.content === botSettings.prefix + 'gitlab') {
        message.channel.send('**Link do gitlaba bota:** https://gitlab.com/MAMAdzwoni/fajnbot \n**Gitlab twórcy**: https://gitlab.com/MAMAdzwoni');
    }
    else if (message.content === botSettings.prefix + 'strona') {
        message.channel.send('**Link do strony bota:** http://fajnbot.tk');
    }
    else if (message.content === botSettings.prefix + 'userinfo') {
    var embed = new Discord.RichEmbed;
    embed.setColor("0x649a4e")
    embed.setAuthor(message.author.username, message.author.avatarURL);
    embed.addField("Nick:", message.author.username, true);
    embed.addField("Tag:", "#" + message.author.discriminator, true);
    embed.addField("ID:", message.author.id, true);
    embed.addField("Status:", message.author.presence.status, true);
    embed.addField("Konto stworzone:", message.author.createdAt, true);
    embed.setThumbnail(message.author.avatarURL);
message.channel.send(embed);
}
});
client.on('message', message => {
      if (message.content === botSettings.prefix + 'invite') {
          var embed = new Discord.RichEmbed;
    embed.setColor("0x649a4e")
    embed.setTitle('Przygarnij bota: ' + inviteurl );
message.channel.send(embed);


    }
  //komenda dla ownera, spam
        else if (message.content === botSettings.prefix + 'lolibomb') {
      if(message.author.id != botSettings.ownerid) return;
        const attachment = new Attachment('img/loli.png');
        // Send the attachment in the message channel
        message.channel.send(attachment);message.channel.send(attachment);message.channel.send(attachment);message.channel.send(attachment);message.channel.send(attachment);message.channel.send(attachment);message.channel.send(attachment);message.channel.send(attachment);message.channel.send(attachment);message.channel.send(attachment);

    }
});

client.login(botSettings.token);

