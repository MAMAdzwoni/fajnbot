# fajnbot

Fajny bot na discorda!
___
[Strona bota](http://fajnbot.tk)
___
Instalacja:
1. Zainstaluj nodejs
2. Sklonuj repo `git clone https://gitlab.com/MAMAdzwoni/fajnbot.git`
3. W pliku botsettings.json ustaw swój token z https://discordapp.com/developers
4. Wpisz w konsole `npm install` - zainstaluje wszystko co potrzebne
5. Zainstaluj forever, przy wywaleniu bota będzie go włączał z powrotem: `npm i forever -g`
6. Zainstaluj screen: `apt install screen`(zazwyczaj)
7. Uruchom bota: `screen -dmS fajnbot forever bot.js`
-  Aby zobaczyć konsolę bota wpisz `screen -r fajnbot`
-  Aby z niej wyjść: Klikasz CTRL+A+D

___
## Niektóre rzeczy zaporzyczone są z innych botów, oto i one:
https://github.com/juby210-PL/bot.js/